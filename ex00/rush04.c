/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush0X.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ademetra <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/02 05:02:16 by ademetra          #+#    #+#             */
/*   Updated: 2020/02/02 05:22:59 by ademetra         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	print_str(char start, char middle, char end, int x)
{
	int i;

	i = 1;
	while (i <= x)
	{
		if (i == 1)
			ft_putchar(start);
		else if (i == x)
			ft_putchar(end);
		else
			ft_putchar(middle);
		i++;
	}
	ft_putchar('\n');
}

void	rush(int x, int y)
{
	int a;

	a = 1;
	while (a <= y)
	{
		if (a == 1)
			print_str('A', 'B', 'C', x);
		else if (a == y)
			print_str('C', 'B', 'A', x);
		else
			print_str('B', ' ', 'B', x);
		a++;
	}
}
